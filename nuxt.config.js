export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "wedding",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "~/assets/global.css",
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    "@nuxtjs/tailwindcss",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "nuxt-route-meta",
    "@nuxtjs/i18n",
    "@nuxtjs/axios",
    [
      "nuxt-mail",
      {
        message: {
          // to: "nopepsi_nohappyend@yahoo.com",
          to: "bob_ludovic@hotmail.com",
        },
        smtp: {
          host: "smtp.mailgun.org",
          port: 587,
          auth: {
            user: "postmaster@sandboxcb24936467ef4854a5142fedbf620c1b.mailgun.org",
            pass: "6d249524a3804a005b26510e6c428aba-c250c684-e18c56d9",
          },
        },
      },
    ],
  ],

  axios: {
    baseURL: 'http://ftp.loche.eu:8888', // Used as fallback if no runtime config is provided
  },


  i18n: {
    locales: [
      { code: "ro", file: "ro.js" },
      { code: "fr", file: "fr.js" },
    ],
    langDir: "~/locales/",
    defaultLocale: "ro",
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    loaders: {
      sass: {
        implementation: require('sass'),
      },
      scss: {
        implementation: require('sass'),
      },
    },
    babel: {
      plugins: [
        [
          'component', {
            libraryName: 'maz-ui',
            styleLibraryName: 'css'
          }
        ]
      ]
    }
  },
};
