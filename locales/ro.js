export default {
  welcome: "Welcome",
  home: "Home",
  contact: "Contact",
  here: "aici",
  common: {
    close: "Inchide"
  },
  title: {
    game: "Termina jocul pentru a accesa invitatia",
    theme: "Ce este... Gatsby?",
    rsvp: "Confirmati prezenta",
    contact: "Contact",
    activities: "Activitati",
    hotels: "Hoteluri & Avioane",
    howTo: "Cum ajungeti la locul nuntii",
  },
  game: {
    link: "http://ftp.loche.eu:8890/",
    hard: "Jocul este mai puternic decat tine sau esti pe telefon?",
    bypass: "Click AICI pentru a accesa invitatia!",
    vlad1: "Multumim, Vlad, pentru grafica jocului si Andrei pentru website",
    vlad2: "Pentru mai mult artwork creat de el, acceseaza link-urile de mai jos:",
    ig: "https://www.instagram.com/blackduckoverlord",
    behance: "https://www.behance.net/blackducko9245",
  },
  intro: {
    hero: "You're Invited",
    romanian: "Apasa aici pentru Romana",
    french: "Clique ici pour Francais",
  },
  welcome: {
    name: "Virginia & Ludovic",
    hero1:
      "Avem deosebita placere sa va invitam la nunta noastra care va avea loc, <span class='underline'>sambata, 24 septembrie 2022</span>, In Bourgogne, Franta. <span class='underline'>Cununia religioasa</span> se va desfasura la biserica Savigny-les-Beaunes la orele <span class='underline'>15:00</span>. <span class='underline'>Masa festiva</span> va avea loc la restaurantul <span class='underline'>Carre Saint Pierre Marey-Les-Fusey</span> la ora <span class='underline'>17:00</span>.",
    hero2:
      "Pentru buna organizare a evenimentului, va rugam sa va confirmati prezenta, apasand",
    deadline: "Confirmati pana la sfarsitul lunii Mai",
    plan: {
      title: "Programul evenimentelor:",
      0: "Vineri, 23 septembrie - Recuperarea oaspetilor din aeroportul Lyon.",
      1: "Vineri, 23 septembrie - Deplasare de la Lyon la hotel.",
      2: "Sambata, 24 septembrie - 15:00 Ceremonia religioasa la biserica din Savigny les Beaunes.",
      3: "Sambata, 24 septembrie - 17:00 Servirea aperitivului la Carre Saint Pierre din Marey-Les-Fusey.",
      4: "Sambata, 24 septembrie - 19:30 Servirea mesei la Carre Saint Pierre.",
      5: "Duminica, 25 septembrie - organizarea unui brunch in Dijon pentru cine doreste sa petreaca a doua zi alaturi de noi (pentru eficientizarea transportului invitatilor este necesara schimbarea cazarii, cu hotel, in Dijon)",
      6: "Luni, 26 septembrie - Transport de la Dijon la aeroportul din Lyon",
    }
  },
  menu: {
    home: "Bun venit",
    rsvp: "RSVP",
    theme: "Tema nuntii",
    contact: "Contact",
    activities: "Activitati dupa nunta",
    hotels: "Hotel si avion",
    howTo: "Cum ajung",
  },
  rsvp: {
    title: "Completati datele pana la sfarsitul lunii Mai",
    name: "Nume si prenume",
    presence: "Prezenta",
    diet: "Dieta specifica",
    nr: "Nr de persoane",
    submit: "Trimite",
    options: {
      none: "Nici una",
      presence: {
        first: "Da",
        second: "Nu",
      },
      diet: {
        no: "Fara regim specific",
        vegetarian: "Vegetarian",
        vegan: "Vegan",
        hallal: "Hallal"
      }
    },
  },
  theme: {
    hero: "Nu stii ce este Gatsby? Ai nevoie de putina inspiratie? Ai mai jos mai multe link-uri care sa te ajute sa fii In ton cu tema nuntii!",
    link: "Apasa aici pentru mai multe informatii",
  },
  contact: {
    hero1: "Intrebari?",
    hero2:
      "Nu sunteti siguri de alegerea tinutei voastre? Sunt copiii bineveniti? Daca aveti Intrebari, adresati-le aici! Le vom primi direct pe e-mail si va raspundem repede. Va multumim!",
    form: {
      name: "Nume",
      email: "Email",
      subject: "Subiect",
      message: "Mesaj",
    },
  },
  activities: {
    0: {
      city: "In Marey-les-Fuseaux",
      description: "Descoperiti vinurile si trufele. In Dijon",
      activity: "Casa trufelor",
      link: "https://www.truffedebourgogne.fr/demonstration-degustation-truffe-beaune-dijon",
    },
    1: {
      city: "In Dijon",
      description:
        "Descoperiti locurile emblematice din Dijon urmand bufnitele.",
      activity: "Calea Bufnitei",
      link: "https://www.destinationdijon.com/moments-a-vivre/le-parcours-de-la-chouette/",
    },
    2: {
      city: "null",
      description:
        "Ce poate fi mai placut decat sa mergi la piata pentru a descoperi produse locale. Tarabele, pline cu de toate, sunt unul din centrele vietii locale. Pregatiti-va toate simturile pentru a va bucura de arome si o atmosfera de neuitat. Aveti sansa sa admirati si structura de fier, din anii 1800, a elegantei piete acoperite.",
      activity: "Les Halles",
      link: "https://goo.gl/maps/PjwA83FnxBko9vjE6",
    },
    3: {
      city: "null",
      description:
        "Considerata o capodopera a arhitecturii gotice din secolul al XIII-lea, este situata In inima centrului vechi conservat al orasului Dijon.",
      activity: "Biserica Notre-Dame",
      link: "https://goo.gl/maps/ZAyHG4TiQXMAB9vE7",
    },
    4: {
      city: "null",
      description:
        "Biserica romano-catolica cu hramul Sfantul Benignus din Dijon. Foarte frumosa In interior.",
      activity: "Biserica Saint-Benigne",
      link: "https://www.cathedrale-dijon.fr/",
    },
    5: {
      city: "null",
      description:
        "Un turn care ofera o panorama superba asupra Dijonului si a Imprejurimilor.",
      activity: "Turnul Philippe le Bon",
      link: "https://en.destinationdijon.com/cultural-heritage/tour-philippe-le-bon/",
    },
    6: {
      city: "null",
      description:
        "Insolitul si fascinantul Muzeu al Vietii Burgundei. Intrarea este libera.",
      activity: "Museul Vietii din Burgundia",
      link: "https://vie-bourguignonne.dijon.fr/",
    },
    7: {
      city: "null",
      description:
        "Terase cu balustrade, iazuri si cascade, aceasta gradina publica, creata In jurul rezervorului de apa construit de inginerul Darcy, evoca stilul italian In inima orasului.",
      activity: "Gradinile Darcy",
      link: "https://goo.gl/maps/9tDZMWz71ckPW8nPA",
    },
    8: {
      city: "null",
      description:
        "Cea mai veche fabrica de turta dulce si magazine din Burgundia, unde veti descoperi o multime de produse locale.",
      activity: "Mulot si Petitjean",
      link: "https://www.mulotpetitjean.com/en/",
    },
    9: {
      city: "null",
      description:
        "Mustar de Dijon care dateaza din 1747 cand copmpania  Maille a fost fondata.",
      activity: "Dijon Mustard",
      link: "https://uk.maille.com/",
    },
  },
  hotels: {
    plane: {
      0: "Va sfatuim sa veniti cu avionul Wizzair, care pleaca din Cluj spre Lyon, vineri, 23 septembrie, la ora 16:50 si care se intoarce luni, 26 septembrie, la ora 18:55.",
      1: "Daca veniti cu acest avion, va va astepta un autobuz care va va aduce la hotel (va rugam sa il precizati pe formularul de confirmare, pentru ca noi sa solicitam un autobuz cu capacitate necesara).",
      2: "Daca doriti sa veniti la date diferite, contactati-ne si va vom oferi consultanta pentru a gasi cea mai buna modalitate de a veni la Dijon.",
    },
    hotels: {
      0: {
        title: "Hoteluri recomandate la locul nuntii.",
        link1: {link: "https://nuits-saint-georges.kyriad.com/fr-fr/", label: "https://nuits-saint-georges.kyriad.com/fr-fr/"},
        link2: {link: "https://www.booking.com/hotel/fr/cottage-nuiton.fr.html?aid=356980;label=gog235jc-1DCAsoTUIOY290dGFnZS1udWl0b25IM1gDaMABiAEBmAENuAEXyAEP2AED6AEBiAIBqAIDuAKP_KeRBsACAdICJDQzYTlkNjEyLTgwMzYtNDE4NC1hNTVkLWVhN2I3NDcwNDFiNtgCBOACAQ;sid=d9fde2eb3a604afe7d8d52d661aa31d8;dist=0&keep_landing=1&sb_price_type=total&type=total&", label: "Cottage Nuiton"},
        link3: {link: "http://lamaisonvigneronne.fr/", label: "http://lamaisonvigneronne.fr/"},
        link4: {link: "https://www.airbnb.com/rooms/33288630", label: "Les nuits du paradis"},
        link5: {link: "https://www.booking.com/hotel/fr/grande-maison-dans-village-viticole-corgoloin.fr.html?aid=356980;label=gog235jc-1DCAsoTUItZ3JhbmRlLW1haXNvbi1kYW5zLXZpbGxhZ2Utdml0aWNvbGUtY29yZ29sb2luSDNYA2jAAYgBAZgBDbgBF8gBD9gBA-gBAYgCAagCA7gCl_6nkQbAAgHSAiQ5NDk1MWYxYy0xOTI2LTQ1YWItYTg0YS02NWVhOTRiMzJmMGXYAgTgAgE;sid=d9fde2eb3a604afe7d8d52d661aa31d8;dist=0&keep_landing=1&sb_price_type=total&type=total&", label: "Grande maison"},
      },
      1: {
        title: "Hoteluri recomandate In Dijon.",
        link1: {link: "https://hotel-ibiscentral-dijon.com/", label: "https://hotel-ibiscentral-dijon.com/"}
      },
    },
  },
  howTo: {
    hotel: {
      title: "Cum sa mergi la Carre-Saint-Pierre:",
      address: "Lieu dit La Chaume, Route de Cassis 21700 Marey-les-Fussey",
    },
    church: {
      title: "Cum sa mergi la biserica:",
      address: "2 Place de l'Eglise 21420 Savigny-les-Beaune",
    },
  },
  modal: {
    successMessage: "Mesajul a fost trimis cu success!",
    successTitle: "Success!",
    errorMessage: "Mesajul nu a putut fi trimis, te rugam sa iei legatura cu Virginia si Ludovic la {mail}",
    errorTitle: "Eroare!",
  }
};
