export default {
  welcome: "Buongiorno",
  home: "Hasza",
  contact: "Pronto",
  here: "ici",
  common: {
    close: "Fermer",
  },
  title: {
    game: "Finis le jeu pour avoir l'invitation",
    theme: "Qu'est ce Gatsby",
    rsvp: "Confirmez votre presence",
    contact: "Contact",
    activities: "Activites apres le mariage",
    hotels: "Logement",
    howTo: "Se rendre au lieu de mariage",
  },
  game: {
    link: "http://ftp.loche.eu:8889",
    hard: "Le jeu est trop dur ou vous etes sur telephone",
    bypass: "Merci de cliquer ici pour acceder a l'invitation",
    vlad1: "Merci a Vlad pour les graphismes du jeu et Andrei pour le site",
    vlad2:
      "Pour voir plus de travail de Vlad vous pouvez acceder sur ces liens ci dessous:",
    ig: "https://www.instagram.com/blackduckoverlord",
    behance: "https://www.behance.net/blackducko9245",
  },
  intro: {
    hero: "You're Invited",
    romanian: "Click here for Romanian",
    french: "Clique ici pour Francais",
  },
  welcome: {
    name: "Virginia & Ludovic",
    hero1:
      "Nous avons le plaisir de vous inviter a notre mariage <span class='underline'>le 24 septembre 2022</span> a l'<span class='underline'>eglise de Savigny les Beaune a 15H</span> et au restaurant <span class='underline'>Carre Saint Pierre a Marey-Les-Fusey a 17H</span>",
    hero2:
      "Afin d'organiser au mieux possible, merci de confirmer votre presence en cliquant sur ce lien",
    deadline: "Merci de confirmer avant fin Mai",
    plan: {
      title: "Planning des evenements:",
      0: "Samedi 24 Septembre - 15h00 Ceremonie religieuse a l'eglise de Savigny les Beaunes",
      1: "Samedi 24 Septembre - 17h00 Vin d'honneur au Carre Saint Pierre a Marey-Les-Fusey",
      2: "Samedi 24 Septembre - 19h30 Debut du repas au Carre Saint Pierre ",
      3: "null",
      4: "null",
      5: "null",
      6: "null",
    },
  },
  menu: {
    home: "Bienvenue",
    rsvp: "RSVP",
    theme: "Theme du mariage",
    contact: "Contact",
    activities: "Activites apres le mariage",
    hotels: "Hotels",
    howTo: "Se rendre au lieu",
  },
  rsvp: {
    title: "Reponse souhaite avant fin Mai",
    name: "Nom et prenom",
    presence: "Presence",
    diet: "Regime particulier",
    nr: "Nombres de personnes",
    submit: "Envoyer",
    options: {
      none: "None",
      presence: {
        first: "Oui",
        second: "Non",
      },
      diet: {
        no: "Pas de regime",
        vegetarian: "Vegetarien",
        vegan: "Vegetalien",
        hallal: "Hallal",
      },
    },
  },
  theme: {
    hero: "Tu ne sais pas ce qu'est Gatsby, tu as besoin d'inspiration, quelques lien ci-dessous pour vous aider",
    link: "Clique ICI pour acceder au lien",
  },
  contact: {
    hero1: "Des questions?",
    hero2:
      "Pas sure du choix de ta robe? Les enfants sont-ils les bienvenus? Si tu as la moindre question ou commentaire, pose-les ici et nous les recevrons directement par email. Merci!",
    form: {
      name: "Nom",
      email: "Email",
      subject: "Objet du message",
      message: "Message",
    },
  },
  activities: {
    0: {
      city: "A Marey-les-Fuseaux",
      description: "Decouverte du vin et truffes A Dijon",
      activity: "La maison aux truffes",
      link: "https://www.truffedebourgogne.fr/demonstration-degustation-truffe-beaune-dijon",
    },
    1: {
      city: "A Dijon",
      description:
        "Decouvrez les lieux iconique de Dijon en suivant les chouettes",
      activity: "Le chemin de la Chouette",
      link: "https://www.destinationdijon.com/moments-a-vivre/le-parcours-de-la-chouette/",
    },
    2: {
      city: "null",
      description:
        "Quoi de mieux que d'aller au marche afin de decouvrir les produits locaux.",
      activity: "Les Halles",
      link: "https://goo.gl/maps/PjwA83FnxBko9vjE6",
    },
    3: {
      city: "null",
      description: "Une magnifique eglise a voir.",
      activity: "eglise Notre-Dame",
      link: "https://goo.gl/maps/ZAyHG4TiQXMAB9vE7",
    },
    4: {
      city: "null",
      description: "Une autre eglise encore plus belle a l'interieur.",
      activity: "eglise Saint-Benigne",
      link: "https://www.cathedrale-dijon.fr/",
    },
    5: {
      city: "null",
      description:
        "Une tour a visiter qui offre un superbe panorama sur Dijon et les alentours.",
      activity: "Tour Philippe le Bon",
      link: "https://en.destinationdijon.com/cultural-heritage/tour-philippe-le-bon/",
    },
    6: {
      city: "null",
      description:
        "Quoi de mieux que de decouvrir la vie Bourguignonne et les traditions",
      activity: "Musee de la vie Bourguignonne",
      link: "https://vie-bourguignonne.dijon.fr/",
    },
    7: {
      city: "null",
      description: "Une petite ballade pour changer d'air",
      activity: "Jardin Darcy",
      link: "https://goo.gl/maps/9tDZMWz71ckPW8nPA",
    },
    8: {
      city: "null",
      description:
        "Decouverte des differents produits bourguignon (pain d'epice, sirop, etc)",
      activity: "Mulot et Petitjean",
      link: "https://www.mulotpetitjean.com/en/",
    },
    9: {
      city: "null",
      description:
        "Que serait Dijon sans la moutarde, ce magasin est un bon endroit pour la decouvrir",
      activity: "Moutarde de Dijon",
      link: "https://uk.maille.com/",
    },
  },
  hotels: {
    hotels: {
      0: {
        title: "Hotels sur le lieu de mariage recommande",
        link1: {
          link: "https://nuits-saint-georges.kyriad.com/fr-fr/",
          label: "https://nuits-saint-georges.kyriad.com/fr-fr/",
        },
        link2: {
          link: "https://www.booking.com/hotel/fr/cottage-nuiton.fr.html?aid=356980;label=gog235jc-1DCAsoTUIOY290dGFnZS1udWl0b25IM1gDaMABiAEBmAENuAEXyAEP2AED6AEBiAIBqAIDuAKP_KeRBsACAdICJDQzYTlkNjEyLTgwMzYtNDE4NC1hNTVkLWVhN2I3NDcwNDFiNtgCBOACAQ;sid=d9fde2eb3a604afe7d8d52d661aa31d8;dist=0&keep_landing=1&sb_price_type=total&type=total&",
          label: "Cottage Nuiton",
        },
        link3: {
          link: "http://lamaisonvigneronne.fr/",
          label: "http://lamaisonvigneronne.fr/",
        },
        link4: {
          link: "https://www.airbnb.com/rooms/33288630",
          label: "Les nuits du paradis",
        },
        link5: {
          link: "https://www.booking.com/hotel/fr/grande-maison-dans-village-viticole-corgoloin.fr.html?aid=356980;label=gog235jc-1DCAsoTUItZ3JhbmRlLW1haXNvbi1kYW5zLXZpbGxhZ2Utdml0aWNvbGUtY29yZ29sb2luSDNYA2jAAYgBAZgBDbgBF8gBD9gBA-gBAYgCAagCA7gCl_6nkQbAAgHSAiQ5NDk1MWYxYy0xOTI2LTQ1YWItYTg0YS02NWVhOTRiMzJmMGXYAgTgAgE;sid=d9fde2eb3a604afe7d8d52d661aa31d8;dist=0&keep_landing=1&sb_price_type=total&type=total&",
          label: "Grande maison",
        },
      },
      1: {
        title: "Hotels recommande a Dijon",
        link1: {
          link: "https://hotel-ibiscentral-dijon.com/",
          label: "https://hotel-ibiscentral-dijon.com/",
        },
      },
    },
  },
  howTo: {
    hotel: {
      title: "Comment se rendre au Carre-Saint-Pierre:",
      address: "Lieu dit La Chaume, Route de Cassis 21700 Marey-les-Fussey",
    },
    church: {
      title: "Comment se rendre a l'eglise:",
      address: "2 Place de l'Eglise 21420 Savigny-les-Beaune",
    },
  },
  modal: {
    successMessage: "Message envoye avec succes!",
    successTitle: "Succes!",
    errorMessage:
      "Le mail n'a pas ete envoye, merci de contacter Virginia et Ludovic a {mail} directement",
    errorTitle: "Erreur!",
  },
};
